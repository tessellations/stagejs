import React from 'react';
import { Stack } from '@mui/material';

import MatterPhysicsEngine from './MatterPhysicsEngine';
import ReactiveController from './ReactiveController';
import CanvasVisualizer from './CanvasVisualizer';
import ThreeVisualizer from "./ThreeVisualizer";

import './styles.css'

const timer = ms => new Promise(res => setTimeout(res, ms))

class VisualizationBenchmark extends React.Component
{
    constructor(props) {
        super(props);

        this.manual_drive = false;
        this.canvas_ref = React.createRef();

        // Maze layout
        // Mazes are stored as a list of vectors with the start and end point x and y coordinates
        // stored as a set of multiples of the grid resolution, so that the maze can be adjusted to
        // accommodate different types and sizes of robots
        // Coordinates are stored a set x0 y0 x1 y1 for each segment
        this.maze_resolution = 80;
        this.maze_cells_x = 10;
        this.maze_cells_y = 8;

        this.maze_goal_x = 5;
        this.maze_goal_y = 4;
        this.maze_goal_length = 1;
        this.maze_goal_width = 1;
        this.maze_goal_height = 0.5;
        this.maze_goal_color = "#10A010";

        this.wall_width = 2 / this.maze_resolution;
        this.wall_color = "#b9a376";
        this.wall_height = 1;

        this.maze_walls = [
            [0, 2, 2, 2],
            [1, 1, 3, 1],
            [3, 1, 3, 6],
            [0, 3, 1, 3],
            [2, 3, 3, 3],
            [2, 3, 2, 4],
            [1, 4, 2, 4],
            [1, 5, 3, 5],
            [1, 5, 1, 7],
            [3, 2, 5, 2],
            [5, 2, 5, 1],
            [4, 1, 4, 0],
            [6, 0, 6, 2],
            [6, 1, 8, 1],
            [9, 0, 9, 2],
            [9, 2, 8, 2],
            [4, 3, 4, 7],
            [4, 3, 8, 3],
            [7, 3, 7, 2],
            [8, 3, 8, 4],
            [8, 4, 10, 4],
            [9, 3, 10, 3],
            [3, 6, 2, 6],
            [2, 6, 2, 8],
            [3, 7, 7, 7],
            [7, 7, 7, 4],
            [7, 5, 9, 5],
            [8, 6, 10, 6],
            [9, 6, 9, 7],
            [8, 7, 8, 8],
            [6, 3, 6, 5],
            [5, 5, 5, 6],
            [5, 5, 6, 5],
            [5, 6, 6, 6],
            [0, 0, 10, 0],
            [10, 0, 10, 8],
            [10, 8, 0, 8],
            [0, 8, 0, 0]
        ];

        // Canvas properties
        this.canvas_width = this.maze_cells_x * this.maze_resolution;
        this.canvas_height = this.maze_cells_y * this.maze_resolution;

        // Robot properties
        this.robot_length = 40 / this.maze_resolution;
        this.robot_width = 30 / this.maze_resolution;
        this.robot_height = 20 / this.maze_resolution;
        this.robot_color = "#232222";

        // Initial position is the center of the first cell
        this.robot_initial_x = 0.5;
        this.robot_initial_y = 0.5;
        this.robot_initial_theta = 0;

        // Sensor properties
        this.center_sensor_x = 20 / this.maze_resolution;
        this.center_sensor_y = 0;
        this.center_sensor_radius = this.robot_width / 2;
        this.center_sensor_color = "#2193ff";

        // Physics engine
        this.engine = {
            ref: null,
            drive_v: 0.75 / this.maze_resolution,
            drive_o: -0.02
        };

        // Visualizers
        this.visualizer_index = 0;
        this.trial_index = 0;
        this.simulation_complete = false;
        this.visualizer_initialized = false;
        this.trial_count = 20;
        this.visualizers = [
            {
                name: "Canvas - 2D",
                max_frame_time: new Array(this.trial_count).fill(Number.MIN_VALUE),
                min_frame_time: new Array(this.trial_count).fill(Number.MAX_VALUE),
                total_frame_count: new Array(this.trial_count).fill(0),
                total_frame_time: new Array(this.trial_count).fill(0),
                all_frame_times: new Array(this.trial_count).fill(new Array(this.frame_interval_count).fill(0)),
                overrun_frame_count: new Array(this.trial_count).fill(0),
            },
            {
                name: "Three.JS",
                max_frame_time: new Array(this.trial_count).fill(Number.MIN_VALUE),
                min_frame_time: new Array(this.trial_count).fill(Number.MAX_VALUE),
                total_frame_count: new Array(this.trial_count).fill(0),
                total_frame_time: new Array(this.trial_count).fill(0),
                all_frame_times: new Array(this.trial_count).fill(new Array(this.frame_interval_count).fill(0)),
                overrun_frame_count: new Array(this.trial_count).fill(0),
            }
        ];

        // Controller
        this.controller = {};
        this.controller_start_time = new Date();

        // FPS and utilization counting
        this.frame_interval_count = 500;
        this.state = {
            max_frame_time: Number.MIN_VALUE,
            min_frame_time: Number.MAX_VALUE,
            total_frame_count: 0,
            total_frame_time: 0,
            all_frame_times: Array(this.frame_interval_count).fill(0),
            overrun_frame_count: 0,
            last_frame_time: new Date(),
            sensor_triggered: false,
            at_goal: false,
            fileDownloadUrl: "",
        };

        this.control = this.control.bind(this);
        this.initialize = this.initialize.bind(this);
        this.computeCurrentFramerate = this.computeCurrentFramerate.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.componentWillUnmount = this.componentWillUnmount.bind(this);
        this.handleKeypress = this.handleKeypress.bind(this);
        this.sensorTriggered = this.sensorTriggered.bind(this);
        this.setVelocity = this.setVelocity.bind(this);
        this.createOutputFile = this.createOutputFile.bind(this);
        this.render = this.render.bind(this);
    }

    async control() {
        for(let i = 0; i < this.visualizers.length; i++) {
            this.visualizer_index = i;

            for(let j = 0; j < this.trial_count;) {
                this.trial_index = j;

                // Create visualizer and engine if they don't exist yet
                if(!this.visualizer_initialized) {
                    this.initialize();
                }
                else {
                    const engine = this.engine.ref;
                    const visualizer = this.visualizers[i];

                    // Load pose
                    const pose = engine.getPose();
                    const x = pose.x;
                    const y = pose.y;
                    const theta = pose.theta;

                    // Exit condition
                    if(this.atGoal(x, y, theta)) {
                        // Save the time constants
                        this.visualizers[i].max_frame_time[j] = this.state.max_frame_time;
                        this.visualizers[i].min_frame_time[j] = this.state.min_frame_time;
                        this.visualizers[i].total_frame_time[j] = this.state.total_frame_time;
                        this.visualizers[i].all_frame_times[j] = this.state.all_frame_times;
                        this.visualizers[i].total_frame_count[j] = this.state.total_frame_count;
                        this.visualizers[i].overrun_frame_count[j] = this.state.overrun_frame_count;

                        // Reset time constraints
                        this.setState({
                            at_goal: true,
                            max_frame_time: Number.MIN_VALUE,
                            min_frame_time: Number.MAX_VALUE,
                            total_frame_count: 0,
                            total_frame_time: 0,
                            all_frame_times: Array(this.frame_interval_count).fill(0),
                            last_frame_time: new Date()
                        });

                        // Delete the engine
                        delete this.engine.ref;

                        // Delete the visualizer
                        delete this.visualizers[i].ref;

                        // Kill the controller too
                        delete this.controller;

                        // Reset the flag
                        this.visualizer_initialized = false;

                        // Update the counter
                        j++;

                        if(j > this.trial_count - 1)
                            break;
                        else
                            continue;
                    } else {
                        // Run the controller
                        if(!this.manual_drive)
                            this.controller.loop();

                        // Update the visualizer
                        if(visualizer.ref)
                            visualizer.ref.render(x, this.maze_cells_y - y, theta);

                        // Update the sensor state
                        this.setState({
                            sensor_triggered: this.sensorTriggered(x, y, theta)
                        });

                        // Update frame count
                        this.computeCurrentFramerate();
                    }
                }

                // Delay
                await timer(1);
            }
        }

        this.simulation_complete = true;
        console.log(`Simulation complete, control() loop terminating`);

        this.createOutputFile();
    }

    initialize() {
        // Create the visualizer
        if(this.visualizer_index === 0)
            this.visualizers[this.visualizer_index].ref = new CanvasVisualizer(this.canvas_ref,
                                                                               this.maze_resolution,
                                                                               this.maze_cells_x,
                                                                               this.maze_cells_y,
                                                                               "#FFFFFF",
                                                                               this.robot_length,
                                                                               this.robot_width,
                                                                               0,
                                                                               this.robot_color,
                                                                               this.center_sensor_x,
                                                                               this.center_sensor_y,
                                                                               this.center_sensor_radius,
                                                                               this.center_sensor_color,
                                                                               this.maze_walls,
                                                                               0,
                                                                               0,
                                                                               this.wall_color,
                                                                               this.maze_goal_x,
                                                                               this.maze_goal_y,
                                                                               this.maze_goal_length,
                                                                               this.maze_goal_width,
                                                                               0,
                                                                               this.maze_goal_color)
        else
            this.visualizers[this.visualizer_index].ref = new ThreeVisualizer(this.canvas_ref,
                                                                              this.maze_resolution,
                                                                              this.maze_cells_x,
                                                                              this.maze_cells_y,
                                                                              "#b9a376",
                                                                              this.robot_length,
                                                                              this.robot_width,
                                                                              this.robot_height,
                                                                              this.robot_color,
                                                                              this.center_sensor_x,
                                                                              this.center_sensor_y,
                                                                              this.center_sensor_radius,
                                                                              this.center_sensor_color,
                                                                              this.maze_walls,
                                                                              this.wall_width,
                                                                              this.wall_height,
                                                                              this.wall_color,
                                                                              this.maze_goal_x,
                                                                              this.maze_goal_y,
                                                                              this.maze_goal_length,
                                                                              this.maze_goal_width,
                                                                              this.maze_goal_height,
                                                                              this.maze_goal_color)

        // Create the engine
        this.engine.ref = new MatterPhysicsEngine(this.maze_cells_x, this.maze_cells_y);
        const engine = this.engine.ref;

        // Create the controller
        this.controller = new ReactiveController(this.maze_cells_x,
                                                 this.maze_cells_y,
                                                 this.maze_resolution,
                                                 this.engine.drive_v,
                                                 this.engine.drive_o,
                                                 this.setVelocity.bind(this),
                                                 () => { return this.state.sensor_triggered },
                                                 () => {
                                                    let pose = this.engine.ref ? this.engine.ref.getPose() : null;

                                                    if(pose)
                                                        return {
                                                            x: pose.x * this.maze_resolution,
                                                            y: pose.y * this.maze_resolution,
                                                            theta: pose.theta
                                                        }
                                                    else
                                                        return {
                                                            x: 0,
                                                            y: 0,
                                                            theta: 0
                                                        }
                                                 });

        this.controller_start_time = new Date();
        this.controller.setup();

        engine.createRobot(this.robot_initial_x,
                        this.maze_cells_y - this.robot_initial_y,
                           this.robot_initial_theta,
                           this.robot_length,
                           this.robot_width);

        for(let i = 0; i < this.maze_walls.length; i++)
        {
            const wall = this.maze_walls[i];

            engine.createWall(wall[0],
                              this.maze_cells_y - wall[1],
                              wall[2],
                              this.maze_cells_y -  wall[3],
                              this.wall_width);
        }

        engine.start();

        this.visualizer_initialized = true;

        // Reset completion indicator
        this.setState({at_goal: false});
    }

    computeCurrentFramerate() {
        // Get time delta since last frame
        const delta_ms = ((new Date().getTime()) - this.state.last_frame_time.getTime());

        const floored_delta = Math.floor(delta_ms);

        if(floored_delta > -1 && floored_delta < this.frame_interval_count) {
            const new_frame_times = this.state.all_frame_times.map((count, index) => {
                if(index === floored_delta)
                    return count + 1;
                else
                    return count;
            });
            this.setState({all_frame_times: new_frame_times});
        }
        else {
            this.setState({overrun_frame_count: this.state.overrun_frame_count + 1});
        }

        this.setState({
            max_frame_time: Math.max(this.state.max_frame_time, delta_ms),
            min_frame_time: Math.min(this.state.min_frame_time, delta_ms),
            total_frame_time: this.state.total_frame_time + delta_ms,
            total_frame_count: this.state.total_frame_count + 1,
            last_frame_time: new Date()
        });
    }

    sensorTriggered(x, y, theta) {
        // Sensor seems to glitch and return true on startup sometimes. This prevents a collision being triggered for a short time after starting the engine
        if(new Date().getTime() - this.controller_start_time.getTime() < 500)
            return false;

        for(let i = 0; i < this.maze_walls.length; i++) {
            const wall = this.maze_walls[i];

            if(this.doesSensorIntersectLine(x,
                y,
                theta,
                this.center_sensor_x,
                this.center_sensor_y,
                this.center_sensor_radius,
                wall[0],
                this.maze_cells_y - wall[1],
                wall[2],
                this.maze_cells_y - wall[3]))
                return true;
        }

        return false;
    }

    /** A workaround for the limits of Matter.JS at this point in time. Collision detection using matter isn't working at this point in time
     * because in the current version a) circular and polygon shapes cause errors, and b) creating composite bodies with constraints also causes
     * errors.
     */
    doesSensorIntersectLine(x, y, theta, x_offset, y_offset, sensor_radius, x0, y0, x1, y1) {
        const angle = Math.atan2(y_offset, x_offset);
        const distance = Math.sqrt(x_offset * x_offset + y_offset * y_offset);
        const sensorCenterX = x + distance * Math.cos(angle + theta);
        const sensorCenterY = y + distance * Math.sin(angle + theta);

        // Define some common functions for working with vectors
        const add = (a, b) => ({x: a.x + b.x, y: a.y + b.y});
        const sub = (a, b) => ({x: a.x - b.x, y: a.y - b.y});
        const dot = (a, b) => a.x * b.x + a.y * b.y;
        const hypot2 = (a, b) => dot(sub(a, b), sub(a, b));

        // Function for projecting some vector a onto b
        function proj(a, b) {
            const k = dot(a, b) / dot(b, b);
            return {x: k * b.x, y: k * b.y};
        }

        function distanceSegmentToPoint(A, B, C) {
            // Compute vectors AC and AB
            const AC = sub(C, A);
            const AB = sub(B, A);

            // Get point D by taking the projection of AC onto AB then adding the offset of A
            const D = add(proj(AC, AB), A);

            const AD = sub(D, A);
            // D might not be on AB so calculate k of D down AB (aka solve AD = k * AB)
            // We can use either component, but choose larger value to reduce the chance of dividing by zero
            const k = Math.abs(AB.x) > Math.abs(AB.y) ? AD.x / AB.x : AD.y / AB.y;

            // Check if D is off either end of the line segment
            if (k <= 0.0) {
                return Math.sqrt(hypot2(C, A));
            } else if (k >= 1.0) {
                return Math.sqrt(hypot2(C, B));
            }

            return Math.sqrt(hypot2(C, D));
        }

        return Math.abs(distanceSegmentToPoint({x: x0, y: y0}, {x: x1, y: y1}, {x: sensorCenterX, y: sensorCenterY})) < sensor_radius;
    }

    atGoal(x, y, theta) {
        const goal_x = this.maze_goal_x;
        const goal_y = this.maze_cells_y - this.maze_goal_y;

        const goal_upper = {
            x: goal_x + this.maze_goal_length / 2,
            y: goal_y + this.maze_goal_width / 2,
        };
        const goal_lower = {
            x: goal_x - this.maze_goal_length / 2,
            y: goal_y - this.maze_goal_width / 2,
        };

        const radius = Math.sqrt((this.robot_length / 2) * (this.robot_length / 2) + (this.robot_width / 2) * (this.robot_width / 2));
        const angle = Math.atan(this.robot_width / this.robot_length);

        const robot0 = {
            x: x + radius * Math.cos(angle + theta),
            y: y + radius * Math.sin(angle + theta)
        };

        const robot1 = {
            x: x - radius * Math.cos(angle - theta),
            y: y + radius * Math.sin(angle - theta)
        };

        const robot2 = {
            x: x - radius * Math.cos(angle + theta),
            y: y - radius * Math.sin(angle + theta)
        };

        const robot3 = {
            x: x + radius * Math.cos(angle - theta),
            y: y - radius * Math.sin(angle - theta)
        };

        const robot_points = [robot0, robot1, robot2, robot3];

        for(let i = 0; i < robot_points.length; i++) {
            const point = robot_points[i];

            if(point.x < goal_upper.x && point.x > goal_lower.x && point.y < goal_upper.y && point.y > goal_lower.y)
                return true;
        }

        return false;
    }

    componentDidMount() {
        window.addEventListener("keypress", this.handleKeypress);

        // Start the control loop
        setTimeout(this.control, 100);
    }

    componentWillUnmount() {
        window.removeEventListener("keypress", this.handleKeypress);
    }

    // Binding for the controller to make use of
    setVelocity(v, o) {
        const engine = this.engine.ref;

        if(engine)
            engine.setRobotVelocity(v, o);
    }

    // Creates the output file for download
    createOutputFile() {
        let data = [];

        // Create header / summary data
        let first = 'Visualizer';
        let second = `Trial`;
        let third = 'Max (ms)';
        let fourth = 'Min (ms)';
        let fifth = `Count above ${this.frame_interval_count}ms`;
        let sixth = `Average (ms)`;
        let seventh = `Median (ms)`;
        let eighth = `SD (ms)`;

        for(let i = 0; i < this.visualizers.length; i++) {
            for(let j = 0; j < this.trial_count; j++)
            {
                const mean = this.visualizers[i].total_frame_time[j] / this.visualizers[i].total_frame_count[j];
                let median = 0, sd = 0, variance = 0;

                // Create a set of all frame values
                let allFrameTimes = [];
                for(let k = 0; k < this.frame_interval_count; k++)
                {
                    const newValues = new Array(this.visualizers[i].all_frame_times[j][k]).fill(k);
                    allFrameTimes = allFrameTimes.concat(newValues);
                }

                // Find median of the set
                const index = Math.floor(allFrameTimes.length / 2);
                if(allFrameTimes.length % 2 !== 0)
                    median = Number(allFrameTimes[index]);
                else
                    median = Number((allFrameTimes[index] + allFrameTimes[index - 1]) / 2);

                // Find standard deviation of the set
                for(let k = 0; k < allFrameTimes.length; k++)
                {
                    variance += ((Number(allFrameTimes[k]) - Number(mean)) ** 2);
                }

                variance = variance / (allFrameTimes.length === 0 ? 1 : allFrameTimes.length);
                sd = Math.sqrt(variance);

                first += `,${this.visualizers[i].name}`;
                second += `,${j}`;
                third += `,${this.visualizers[i].max_frame_time[j].toString()}`;
                fourth += `,${this.visualizers[i].min_frame_time[j].toString()}`;
                fifth += `,${this.visualizers[i].overrun_frame_count[j].toString()}`;
                sixth += `,${mean}`;
                seventh += `,${median}`;
                eighth += `,${sd}`;
            }
        }

        first += '\r\n';
        second += '\r\n';
        third += '\r\n';
        fourth += '\r\n';
        fifth += '\r\n';
        sixth += '\r\n';
        seventh += '\r\n';
        eighth += '\r\n';

        data.push(first);
        data.push(second);
        data.push(third);
        data.push(fourth);
        data.push(fifth);
        data.push(sixth);
        data.push(seventh);
        data.push(eighth);
        data.push(',\r\n');

        // Create blob
        const blob = new Blob(data, {type:'text/plain'});
        this.setState({fileDownloadUrl: URL.createObjectURL(blob)});
    }

    handleKeypress(event) {
        const key = event.code.toString().replace('Key', '').toLowerCase();
        const engine = this.engine.ref;
        const v = this.engine.drive_v;
        const o = this.engine.drive_o;

        switch(key)
        {
            case 'q':
                this.manual_drive = !this.manual_drive;
                break;
            case 'w':
                if(this.manual_drive)
                    engine.setRobotVelocity(v, 0);
                break;
            case 's':
                if(this.manual_drive)
                    engine.setRobotVelocity(-v, 0);
                break;
            case 'a':
                if(this.manual_drive)
                    engine.setRobotVelocity(0, o);
                break;
            case 'd':
                if(this.manual_drive)
                    engine.setRobotVelocity(0, -o);
                break;
            case 'space':
                engine.setRobotVelocity(0, 0);
                break;
            default:
                console.log(`Unhandled key press for key ${key}`);
                break;
        }
    }

    render() {
        const downloadButton = this.state.fileDownloadUrl.length === 0 ? <p /> : <button className={"download-button"} onClick={() => {
            const link = document.createElement('a');
            link.download = `dimensional_simulation_results_${new Date().toISOString().replace(':', '.')}.csv`;
            link.href = this.state.fileDownloadUrl;
            link.addEventListener('click', (e) => {
                setTimeout(() => URL.revokeObjectURL(link.href), 30 * 1000);
            });
            link.click();
            this.setState({fileDownloadUrl: ""});
        }}>
            Download Performance Data
        </button>;

        const infoComponents = this.simulation_complete ? [] : [
            <p key={"1"}>Currently evaluating: {this.visualizers[this.visualizer_index].name}</p>,
            <p key={"2"}>Trial number {this.trial_index + 1} / {this.trial_count}</p>,
            <p key={"3"}>Average frame time: {this.state.total_frame_time / this.state.total_frame_count}</p>,
            <p key={"4"}>Maximum frame time: {this.state.max_frame_time}</p>,
            <p key={"5"}>Minimum frame time: {this.state.min_frame_time}</p>,
            <br key={"6"} />,
            <Stack key={"7"} direction={"row"} alignItems={"flex-start"} justifyContent={"flex-start"} spacing={2}>
                <div className={this.state.sensor_triggered ? "green-indicator" : "gray-indicator"} />
            </Stack>,
            <br key={"8"}/>,
            <p key={"9"}>{this.state.at_goal ? "Done! Cleaning up..." : ""}</p>,
            <p key={"10"}>{this.manual_drive ? "Manual drive enabled" : ""}</p>
        ];

        return (
        <div className={"container"}>
            <Stack direction={"row"} alignItems={"flex-start"} justifyContent={"flex-start"} spacing={2}>
                <canvas id={"canvas0"} ref={this.canvas_ref} width={this.canvas_width} height={this.canvas_height}/>
                <Stack direction={"column"} alignItems={"flex-start"} justifyContent={"flex-start"} spacing={2} sx={{width: "300px"}}>
                    {infoComponents}
                    <br />
                    {downloadButton}
                </Stack>
                <canvas id={"canvas1"} width={this.canvas_width} height={this.canvas_height}/>
            </Stack>
        </div>)
    }
}

export default VisualizationBenchmark;