import React from 'react';
import ReactDOM from 'react-dom/client';
import VisualizationBenchmark from './VisualizationBenchmark';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <VisualizationBenchmark />
  </React.StrictMode>
);
