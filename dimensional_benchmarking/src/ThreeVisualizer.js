import * as Three from "three";
import { OrbitControls } from "three/addons/controls/OrbitControls.js";

class ThreeVisualizer
{
    constructor(ref,
                resolution_multiplier,
                stage_length,
                stage_width,
                stage_color,
                robot_length,
                robot_width,
                robot_height,
                robot_color,
                sensor_x,
                sensor_y,
                sensor_radius,
                sensor_color,
                wall_coordinates,
                wall_width,
                wall_height,
                wall_color,
                goal_x,
                goal_y,
                goal_length,
                goal_width,
                goal_height,
                goal_color)
    {
        // Initialization
        this.canvas_ref = ref;
        this.resolution_multiplier = resolution_multiplier;
        this.stage_length = stage_length;
        this.stage_width = stage_width;
        this.stage_color = stage_color;
        this.robot_length = robot_length;
        this.robot_width = robot_width;
        this.robot_height = robot_height;
        this.robot_color = robot_color;
        this.sensor_x = sensor_x;
        this.sensor_y = sensor_y;
        this.sensor_radius = sensor_radius;
        this.sensor_color = sensor_color;
        this.wall_coordinates = wall_coordinates
        this.wall_width = wall_width;
        this.wall_height = wall_height
        this.wall_color = wall_color;
        this.goal_x = goal_x;
        this.goal_y = goal_y;
        this.goal_length = goal_length;
        this.goal_width = goal_width;
        this.goal_height = goal_height;
        this.goal_color = goal_color;

        // Create an empty scene
        this.scene = new Three.Scene();

        // Create a basic perspective camera
        this.camera = new Three.PerspectiveCamera( 75, this.stage_length / this.stage_width, 0.1, 1000 );

        // Start with a top-down view
        this.camera.position.x = 0;
        this.camera.position.y = 0;
        this.camera.position.z = 2 * this.wall_height;
        this.camera.lookAt(this.stage_length / 2, this.stage_width / 2, 0);

        // Create the floor
        this.stage_geometry = new Three.PlaneGeometry(this.stage_length, this.stage_width);
        this.stage_material = new Three.MeshBasicMaterial({ color: "#409894" });
        this.stage = new Three.Mesh(this.stage_geometry, this.stage_material);
        this.stage.position.x = this.stage_length / 2;
        this.stage.position.y = this.stage_width / 2;
        // this.stage.receiveShadow = true;
        this.scene.add(this.stage);

        // Create the robot body
        this.robot_geometry = new Three.BoxGeometry(this.robot_length, this.robot_width, this.robot_height);
        this.robot_material = new Three.MeshBasicMaterial({ color: this.robot_color });
        this.robot = new Three.Mesh(this.robot_geometry, this.robot_material);
        // this.robot.castShadow = true;
        // this.robot.receiveShadow = true;
        this.scene.add(this.robot);

        // Create the sensor body
        this.sensor_geometry = new Three.CylinderGeometry(this.sensor_radius, this.sensor_radius, this.robot_height / 2);
        this.sensor_material = new Three.MeshBasicMaterial({ color: this.sensor_color, transparent: true, opacity: 0.5 });
        this.sensor = new Three.Mesh(this.sensor_geometry, this.sensor_material);
        this.sensor.rotation.x = -Math.PI / 2;
        // this.sensor.castShadow = true;
        this.scene.add(this.sensor);

        // Create the goal body
        this.goal_geometry = new Three.BoxGeometry(this.goal_length, this.goal_width, this.goal_height);
        this.goal_material = new Three.MeshBasicMaterial({ color: this.goal_color });
        this.goal = new Three.Mesh(this.goal_geometry, this.goal_material);
        this.goal.position.x = this.goal_x;
        this.goal.position.y = this.goal_y;
        this.goal.position.z = this.goal_height / 2;
        this.scene.add(this.goal);

        // // Create the lighting
        // this.goal_light = new Three.PointLight(0xffffff, 1, 10);
        // this.goal_light.position.set( this.stage_length / 2, this.stage_width / 2, 2 * this.wall_height );
        // this.goal_light.castShadow = true;
        // this.scene.add(this.goal_light);
        //
        // const pointLightHelper = new Three.PointLightHelper( this.goal_light, 1 );
        // this.scene.add( pointLightHelper );

        // // Shadow properties for the light
        // this.goal_light.shadow.mapSize.width = 512;
        // this.goal_light.shadow.mapSize.height = 512;
        // this.goal_light.shadow.camera.near = 0.5;
        // this.goal_light.shadow.camera.far = 500;

        // Create the walls
        this.wall_coordinates.forEach((wall) => {
            const wall_x = Math.abs(wall[0] - wall[2]) === 0 ? this.wall_width : Math.abs(wall[0] - wall[2]);
            const wall_y = Math.abs(wall[1] - wall[3]) === 0 ? this.wall_width : Math.abs(wall[1] - wall[3]);

            let wall_geometry = new Three.BoxGeometry(wall_x, wall_y, this.wall_height);
            let wall_material = new Three.MeshBasicMaterial({ color: this.wall_color });
            let wall_mesh =  new Three.Mesh(wall_geometry, wall_material);
            wall_mesh.position.x = (wall[0] + wall[2]) / 2;
            wall_mesh.position.y = (wall[1] + wall[3]) / 2;
            wall_mesh.position.z = this.wall_height / 2;
            // wall_mesh.castShadow = true;
            // wall_mesh.receiveShadow = true;

            this.scene.add(wall_mesh);
        });

        // Create renderer
        this.renderer = new Three.WebGLRenderer( { canvas: document.getElementById("canvas1") } );
        this.renderer.setSize(800, 600);
        this.renderer.setClearColor("#303030");
        // this.renderer.shadowMap.enabled = true;
        // this.renderer.shadowMap.type = Three.BasicShadowMap;

        // Create controls
        this.controls = new OrbitControls(this.camera, document.getElementById("canvas1"));
    }

    render(robot_x,
           robot_y,
           robot_theta)
    {
        // Update robot pose
        if(this.robot) {
            this.robot.position.x = robot_x;
            this.robot.position.y = robot_y;
            this.robot.position.z = this.robot_height / 2;
            this.robot.rotation.x = 0;
            this.robot.rotation.y = 0;
            this.robot.rotation.z = -robot_theta;

            // Update the sensor pose
            this.sensor.position.x = robot_x + (this.sensor_x * Math.cos(-robot_theta)) + (this.sensor_y * Math.sin(-robot_theta));
            this.sensor.position.y = robot_y + (this.sensor_x * Math.sin(-robot_theta)) + (this.sensor_y * Math.cos(-robot_theta));
            this.sensor.position.z = this.robot_height / 2;

            // Update the camera position too
            // if(this.camera) {
            //     this.camera.position.x = this.robot.position.x - Math.cos(-this.robot.rotation.y);
            //     this.camera.position.y = this.robot.position.y + 1;
            //     this.camera.position.z = this.robot.position.z - Math.sin(-this.robot.rotation.y);
            //     this.camera.lookAt(this.robot.position.x, this.robot.position.y, this.robot.position.z);
            // }
        }

        // Update the controls
        if(this.controls) {
            this.controls.target.x = robot_x;
            this.controls.target.y = robot_y;
            this.controls.target.z = this.robot_height;
            this.controls.update();
        }

        // Render a new frame
        if(this.renderer) {
            // Render the scene
            this.renderer.render(this.scene, this.camera);
        }
    }
}

export default ThreeVisualizer;