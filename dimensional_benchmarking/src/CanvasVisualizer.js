
class CanvasVisualizer
{
    constructor(ref,
                resolution_multiplier,
                stage_length,
                stage_width,
                stage_color,
                robot_length,
                robot_width,
                robot_height,
                robot_color,
                sensor_x,
                sensor_y,
                sensor_radius,
                sensor_color,
                wall_coordinates,
                wall_width,
                wall_height,
                wall_color,
                goal_x,
                goal_y,
                goal_length,
                goal_width,
                goal_height,
                goal_color)
    {
        // Initialization
        this.canvas_ref = ref;
        this.resolution_multiplier = resolution_multiplier;
        this.stage_length = stage_length;
        this.stage_width = stage_width;
        this.stage_color = stage_color;
        this.robot_length = robot_length;
        this.robot_width = robot_width;
        this.robot_color = robot_color;
        this.sensor_x = sensor_x;
        this.sensor_y = sensor_y;
        this.sensor_radius = sensor_radius;
        this.sensor_color = sensor_color;
        this.wall_coordinates = wall_coordinates;
        this.wall_color = wall_color;
        this.goal_x = goal_x;
        this.goal_y = goal_y;
        this.goal_length = goal_length;
        this.goal_width = goal_width;
        this.goal_color = goal_color;

        this.render = this.render.bind(this);
    }

    /** Main render method. Will be called on a fixed period to render the robot, walls and goal
     *
     */
    render(robot_x,
           robot_y,
           robot_theta)
    {
        // Load the context
        let canvas, context;
        try {
            canvas = this.canvas_ref.current;
            context = canvas ? canvas.getContext("2d") : null;

            // Make sure that context is valid
            if(!context) {
                console.error("Error in render(): context does not exist");
                return;
            }
        } catch (err) {
            console.error(`Error loading canvas and context: ` + err.toString());
            return;
        }

        // Clear the canvas
        context.clearRect(0, 0, this.stage_length * this.resolution_multiplier, this.stage_width * this.resolution_multiplier);

        // Render the stage
        this.drawRectangle(context,
            this.stage_color,
            (this.stage_length / 2) * this.resolution_multiplier,
            (this.stage_width / 2) * this.resolution_multiplier,
            0,
            this.stage_length * this.resolution_multiplier,
            this.stage_width * this.resolution_multiplier);

        // Render the walls
        for(let i = 0; i < this.wall_coordinates.length; i++)
        {
            const segment = this.wall_coordinates[i];
            this.drawLine(context,
                this.wall_color,
                segment[0] * this.resolution_multiplier,
                (this.stage_width - segment[1]) * this.resolution_multiplier,
                segment[2] * this.resolution_multiplier,
                (this.stage_width - segment[3]) * this.resolution_multiplier);
        }

        // Render the goal
        this.drawRectangle(context,
                           this.goal_color,
                           this.goal_x * this.resolution_multiplier,
                           (this.stage_width - this.goal_y) * this.resolution_multiplier,
                           0,
                           this.goal_length * this.resolution_multiplier,
                           this.goal_width * this.resolution_multiplier);

        // Render the sensor
        this.drawSensor(context,
            this.sensor_color,
            robot_x * this.resolution_multiplier,
            (this.stage_width - robot_y) * this.resolution_multiplier,
            robot_theta,
            this.sensor_x * this.resolution_multiplier,
            this.sensor_y * this.resolution_multiplier,
            this.sensor_radius * this.resolution_multiplier);

        // Render the robot
        this.drawRectangle(context,
            this.robot_color,
            robot_x * this.resolution_multiplier,
            (this.stage_width - robot_y) * this.resolution_multiplier,
            robot_theta,
            this.robot_length * this.resolution_multiplier,
            this.robot_width * this.resolution_multiplier);
    }

    drawRectangle(context, color, x, y, theta, length_x, width_y) {
        const radius = Math.sqrt((length_x / 2) * (length_x / 2) + (width_y / 2) * (width_y / 2));
        const angle = Math.atan(width_y / length_x);

        const p0 = {
            x: x + radius * Math.cos(angle + theta),
            y: y + radius * Math.sin(angle + theta)
        };

        const p1 = {
            x: x - radius * Math.cos(angle - theta),
            y: y + radius * Math.sin(angle - theta)
        };

        const p2 = {
            x: x - radius * Math.cos(angle + theta),
            y: y - radius * Math.sin(angle + theta)
        };

        const p3 = {
            x: x + radius * Math.cos(angle - theta),
            y: y - radius * Math.sin(angle - theta)
        };

        const points = [p0, p1, p2, p3];

        for(let i = 0; i < points.length; i++)
        {
            for(let j = 0; j < points.length; j++)
            {
                context.fillStyle = color;

                if(i === j)
                    continue;
                else
                {
                    context.beginPath();
                    context.moveTo(x, y);
                    context.lineTo(points[i].x, points[i].y);
                    context.lineTo(points[j].x, points[j].y);
                    context.closePath();
                    context.fill();
                }
            }
        }
    }

    drawLine(context, color, x0, y0, x1, y1) {
        context.beginPath();
        context.moveTo(x0, y0);
        context.lineTo(x1, y1);
        context.closePath();
        context.fillStyle = color;
        context.stroke();
    }

    drawSensor(context, color, x, y, theta, x_offset, y_offset, radius) {
        const angle = Math.atan2(y_offset, x_offset);
        const distance = Math.sqrt(x_offset * x_offset + y_offset * y_offset);
        const adjustedX = x + distance * Math.cos(angle + theta);
        const adjustedY = y + distance * Math.sin(angle + theta);

        context.moveTo(adjustedX, adjustedY);
        context.arc(adjustedX, adjustedY, radius, 0, 2 * Math.PI);
        context.fillStyle = color;
        context.fill();
    }
}

export default CanvasVisualizer;