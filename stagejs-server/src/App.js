import './App.css';
import React from "react";
import Matter from "matter-js";

import world_demo from './world_demo.json';

// TODO: fix actor reset on resize
// TODO: add ability to set theta for all objects
// TODO: y axis is inverted (0=top). Invert Y axis

class App extends React.Component
{
    constructor(props) {
        super(props);

        this.boxRef = React.createRef();
        this.canvasRef = React.createRef();

        this.world = world_demo;
        this.borderComponents = [];
        this.dynamicComponents = [];
        this.actors = [];

        this.state = {
            world_x_min: 0,
            world_x_max: 0,
            world_y_min: 0,
            world_y_max: 0,
            scaleFactor: 0
        };

        this.componentDidMount = this.componentDidMount.bind(this);
        this.componentWillUnmount = this.componentWillUnmount.bind(this);
        this.updateWorldConstraintsFromLayout = this.updateWorldConstraintsFromLayout.bind(this);
        this.createBordersFromLayout = this.createBordersFromLayout.bind(this);
        this.createDynamicsFromLayout = this.createDynamicsFromLayout.bind(this);
        this.createActorsFromLayout = this.createActorsFromLayout.bind(this);
        this.handleResize = this.handleResize.bind(this);
        this.handleKeypress = this.handleKeypress.bind(this);
        this.render = this.render.bind(this);
    }

    componentDidMount() {
        // Create the engine and renderer
        this.engine = Matter.Engine.create({});
        this.engine.world.gravity.y = 0;

        this.renderer = Matter.Render.create({
            element: this.boxRef.current,
            engine: this.engine,
            canvas: this.canvasRef.current,
            options: {
                background: "transparent",
                wireframes: false
            }
        });

        // Create the initial world constraints
        this.updateWorldConstraintsFromLayout();

        // Create the initial world borders
        this.createBordersFromLayout();
        Matter.World.add(this.engine.world, this.borderComponents);

        // Create the initial dynamic objects
        this.createDynamicsFromLayout();
        Matter.World.add(this.engine.world, this.dynamicComponents);

        // Create the actors
        this.createActorsFromLayout();
        Matter.World.add(this.engine.world, this.actors.map(actor => actor.ref));

        // Render the world
        Matter.Runner.run(this.engine);
        Matter.Render.run(this.renderer);

        window.addEventListener("keypress", this.handleKeypress);

        setTimeout(this.handleResize, 10);
    }

    componentWillUnmount() {
        window.removeEventListener("keypress", this.handleKeypress);
    }

    createBordersFromLayout() {
        let { width, height } = this.boxRef.current.getBoundingClientRect();
        
        console.log(`Starting border creation:`);

        if((this.state.world_x_max - this.state.world_x_min) < 0.001) {
            console.warn(`Error in border creation: Invalid world x boundaries`);
            return;
        }
        if((this.state.world_y_max - this.state.world_y_min) < 0.001) {
            console.warn(`Error in border creation: Invalid world y boundaries`);
            return;
        }
        
        for(let i = 0; i < this.world.borders.length; i++) {
            const border = this.world.borders[i];
            
            const scaledX = (parseFloat(border.x) - this.state.world_x_min) / (this.state.world_x_max - this.state.world_x_min);
            const scaledY = (parseFloat(border.y) - this.state.world_y_min) / (this.state.world_y_max - this.state.world_y_min);
            const scaledXWidth = parseFloat(border.x_width) / (this.state.world_x_max - this.state.world_x_min);
            const scaledYWidth = parseFloat(border.y_width) / (this.state.world_y_max - this.state.world_y_min);

            console.log(`Border: X${border.x} Y${border.y} dX${border.x_width} dY${border.y_width} -> X${scaledX} Y${scaledY} dX${scaledXWidth} dY ${scaledYWidth}`);

            const newBorderRectangle = Matter.Bodies.rectangle(
                width * scaledX,
                height * scaledY,
                width * scaledXWidth,
                height * scaledYWidth,
                {
                    isStatic: true,
                    label: `border_${i}`,
                    render: {
                        fillStyle: "black"
                    }
                });

            this.borderComponents.push(newBorderRectangle);
        }
    }

    createDynamicsFromLayout() {
        // Create dynamic components, i.e, non-fixed objects like furniture or obstacles
        let { width, height } = this.boxRef.current.getBoundingClientRect();
        for(let i = 0; i < this.world.dynamic.length; i++) {
            const dynamic = this.world.dynamic[i];

            const scaledX = (parseFloat(dynamic.x) - this.state.world_x_min) / (this.state.world_x_max - this.state.world_x_min);
            const scaledY = (parseFloat(dynamic.y) - this.state.world_y_min) / (this.state.world_y_max - this.state.world_y_min);
            const scaledXWidth = parseFloat(dynamic.x_width) / (this.state.world_x_max - this.state.world_x_min);
            const scaledYWidth = parseFloat(dynamic.y_width) / (this.state.world_y_max - this.state.world_y_min);

            const newDynamicObject = Matter.Bodies.rectangle(
                width * scaledX,
                height * scaledY,
                width * scaledXWidth,
                height * scaledYWidth,
                {
                    label: `dynamic_${dynamic.label}_${i}`,
                    restitution: parseFloat(dynamic.restitution),
                    friction: parseFloat(dynamic.friction),
                    frictionAir: parseFloat(dynamic.air_friction),
                    render: {
                        fillStyle: "orange"
                    }
                });

            this.dynamicComponents.push(newDynamicObject);
        }
    }

    createActorsFromLayout() {
        // Create actors
        let { width, height } = this.boxRef.current.getBoundingClientRect();
        for(let i = 0; i < this.world.actors.length; i++) {
            const actor = this.world.actors[i];

            const scaledX = (parseFloat(actor.origin.x) - this.state.world_x_min) / (this.state.world_x_max - this.state.world_x_min);
            const scaledY = (parseFloat(actor.origin.y) - this.state.world_y_min) / (this.state.world_y_max - this.state.world_y_min);
            const scaledXWidth = parseFloat(actor.dimensions.length) / (this.state.world_x_max - this.state.world_x_min);
            const scaledYWidth = parseFloat(actor.dimensions.width) / (this.state.world_y_max - this.state.world_y_min);

            const newBorderRectangle = Matter.Bodies.rectangle(
                width * scaledX,
                height * scaledY,
                width * scaledXWidth,
                height * scaledYWidth,
                {
                    label: `actor_${actor.label}`,
                    render: {
                        fillStyle: "gray"
                    }
                });

            this.actors.push({
                ref: newBorderRectangle,
                label: actor.label,
                x: parseFloat(actor.origin.x),
                y: parseFloat(actor.origin.y),
                keyboard_bindings: actor.keyboard_bindings
            });
        }
    }

    updateWorldConstraintsFromLayout() {
        // Create the world x and y min and max values based on padding and the world layout, which determines the minimum zoom level
        let { width, height } = this.boxRef.current.getBoundingClientRect();
        const widthRatio = width / height;
        console.log(`Starting world constraint scaling (ratio: ${widthRatio})`);

        let xMax = 0, xMin = 0, yMax = 0, yMin = 0;
        for(let i = 0; i < this.world.borders.length; i++) {
            const border = this.world.borders[i];
            const vertices = [
                {x: parseFloat(border.x) + parseFloat(border.x_width) / 2.0, y: parseFloat(border.y) + parseFloat(border.y_width) / 2.0},
                {x: parseFloat(border.x) - parseFloat(border.x_width) / 2.0, y: parseFloat(border.y) + parseFloat(border.y_width) / 2.0},
                {x: parseFloat(border.x) + parseFloat(border.x_width) / 2.0, y: parseFloat(border.y) - parseFloat(border.y_width) / 2.0},
                {x: parseFloat(border.x) - parseFloat(border.x_width) / 2.0, y: parseFloat(border.y) - parseFloat(border.y_width) / 2.0}
            ];

            for(let j = 0; j < 4; j++) {
                const vertex = vertices[i];

                if(i === 0 && j === 0) {
                    xMax = vertex.x;
                    yMax = vertex.y;
                    xMin = vertex.x;
                    yMin = vertex.y;
                }
                else {
                    xMax = Math.max(vertex.x, xMax);
                    yMax = Math.max(vertex.y, yMax);
                    xMin = Math.min(vertex.x, xMin);
                    yMin = Math.min(vertex.y, yMin);
                }
            }
        }

        const xDelta = xMax - xMin;
        const yDelta = yMax - yMin;
        const scaleWidthRatio = xDelta / yDelta;

        // We are working with 2 aspect ratios: one for the screen, and one for the world map.
        // This transforms the scaling of the world map to the aspect ratio of the screen
        if(scaleWidthRatio > widthRatio) {
            const newYDelta = yDelta * (scaleWidthRatio / widthRatio);
            const y = (yMax - yMin) / 2.0;

            yMax = y + (newYDelta / 2.0);
            yMin = y - (newYDelta / 2.0);

        } else {
            const newXDelta = xDelta * (widthRatio / scaleWidthRatio);
            const x = (xMax - xMin) / 2.0;

            xMax = x + (newXDelta / 2.0);
            xMin = x - (newXDelta / 2.0);
        }

        // Create padding for arena
        const padding = parseFloat(this.world.padding) * Math.min((xMax - xMin), (yMax - yMin));
        const worldBoundaryXMin = xMin - padding;
        const worldBoundaryXMax = xMax + padding;
        const worldBoundaryYMin = yMin - padding;
        const worldBoundaryYMax = yMax + padding;

        console.log(`World xMin: ${worldBoundaryXMin} xMax: ${worldBoundaryXMax} yMin: ${worldBoundaryYMin} yMax: ${worldBoundaryYMax}`);

        this.setState({
            world_x_min: worldBoundaryXMin,
            world_x_max: worldBoundaryXMax,
            world_y_min: worldBoundaryYMin,
            world_y_max: worldBoundaryYMax
        });
    }

    handleResize() {
        let { width, height } = this.boxRef.current.getBoundingClientRect();

        // Dynamically update canvas and bounds
        this.renderer.bounds.max.x = width;
        this.renderer.bounds.max.y = height;
        this.renderer.options.width = width;
        this.renderer.options.height = height;
        this.renderer.canvas.width = width;
        this.renderer.canvas.height = height;

        this.updateWorldConstraintsFromLayout();

        // Update border sizes based on the new size of the window
        Matter.World.remove(this.engine.world, this.borderComponents);
        this.borderComponents = [];
        this.createBordersFromLayout();
        Matter.World.add(this.engine.world, this.borderComponents);

        // Update dynamic object sizes based on the new size of the window
        Matter.World.remove(this.engine.world, this.dynamicComponents);
        this.dynamicComponents = [];
        this.createDynamicsFromLayout();
        Matter.World.add(this.engine.world, this.dynamicComponents);

        // Update actors
        // TODO: this will reset robot pose on resize. Fix.
        Matter.World.remove(this.engine.world, this.actors.map(actor => actor.ref));
        this.actors = [];
        this.createActorsFromLayout();
        Matter.World.add(this.engine.world, this.actors.map(actor => actor.ref));
    }

    handleKeypress(event) {
        const key = event.code.toString().replace('Key', '').toLowerCase();

        console.log(`Processing key press ${key}`);

        for(let i = 0; i < this.actors.length; i++) {
            const actor = this.actors[i];

            for(let j = 0; j < actor.keyboard_bindings.length; j++) {
                const binding = actor.keyboard_bindings[j];

                if(key === binding.key) {
                    console.log(`Executing keyboard command for key ${key}`)

                    // We must use the rotation of the body to calculate the velocity of the body
                    const theta = actor.ref.angle + (2.0 * Math.PI);
                    Matter.Body.setVelocity(actor.ref, {x: Math.cos(theta) * parseFloat(binding.v), y: Math.sin(theta) * parseFloat(binding.v)});
                    Matter.Body.setAngularVelocity(actor.ref, parseFloat(binding.o));
                }
            }
        }
    }

    render() {
        return (
            <div
                ref={this.boxRef}
                style={{
                    position: "absolute",
                    overflow: "hidden",
                    top: 0,
                    left: 0,
                    width: "100%",
                    height: "100%",
                    zIndex: 999
                }}
                onResize={this.handleResize}
            >
                <canvas ref={this.canvasRef} />
            </div>
        );
    }
}

export default App;