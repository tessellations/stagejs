import {
    b2Vec2,
    b2World,
    b2BodyType,
    b2PolygonShape,
    b2EdgeShape,
} from "@box2d/core";

class Box2DPhysicsEngine
{
    constructor(screen_width, screen_height) {
        this.factor = 100;

        this.robot_ref = {};
        this.wall_refs = [];
        this.gravity = {};
        this.world = {};
        this.interval = {};

        // World creation
        this.gravity = new b2Vec2(0.0, 0.0);
        this.world = b2World.Create(this.gravity);

        // Bindings
        this.createRobot = this.createRobot.bind(this);
        this.createWall = this.createWall.bind(this);
        this.start = this.start.bind(this);
        this.setRobotVelocity = this.setRobotVelocity.bind(this);
        this.getPose = this.getPose.bind(this);
        this.destroy = this.destroy.bind(this);
    }

    createRobot(x,
                y,
                theta,
                length,
                width)
    {
        const robot_body = this.world.CreateBody({type: b2BodyType.b2_dynamicBody, position: { x: x / this.factor, y: y / this.factor }});
        const robot_shape = new b2PolygonShape();
        robot_shape.SetAsBox(length / (2 * this.factor), width / (2 * this.factor), {x: 0, y: 0}, 0);
        robot_body.CreateFixture({shape: robot_shape, density: 0.5, restitution: 0, friction: 0.5});
        robot_body.SetTransformXY(x / this.factor, y / this.factor, theta);

        this.robot_ref = robot_body;
    }

    createWall(start_x, start_y, end_x, end_y, width) {
        const body = this.world.CreateBody({type: b2BodyType.b2_staticBody});
        const shape = new b2EdgeShape();
        shape.SetTwoSided(new b2Vec2(start_x / this.factor, start_y / this.factor), new b2Vec2(end_x / this.factor, end_y / this.factor));
        body.CreateFixture({shape: shape, friction: 0.5});

        this.wall_refs.push(body);
    }

    start() {
        const time_step = 1.0 / 60.0;    // 60 FPS

        this.interval = setInterval(() => {
            this.world.Step(time_step, 6, 2);
        }, time_step * 1000);
    }

    setRobotVelocity(v, o) {
        if(this.robot_ref) {
            try {
                const angle = this.robot_ref.GetAngle();
                this.robot_ref.SetLinearVelocity({x: v * Math.cos(angle), y: v * Math.sin(angle)});
                this.robot_ref.SetAngularVelocity(o);
            } catch (e) {
                console.log(`Error in setRobotVelocity: ${e}`)
            }

        } else {
            console.warn(`Failed to set robot velocity`);
        }
    }

    getPose() {
        if(this.robot_ref) {
            try {
                const poseXY = this.robot_ref.GetWorldCenter();
                const theta = this.robot_ref.GetAngle();

                if(!poseXY.x || !poseXY.y) {
                    return {
                        x: 0,
                        y: 0,
                        theta: 0
                    }
                } else {
                    return {
                        x: poseXY.x * this.factor,
                        y: poseXY.y * this.factor,
                        theta: theta
                    }
                }
            } catch(e) {
                return {
                    x: 0,
                    y: 0,
                    theta: 0
                }
            }
        } else {
            return {
                x: 0,
                y: 0,
                theta: 0
            }
        }
    }

    destroy() {
        clearInterval(this.interval);

        // this.robot_ref = {};
        // this.wall_refs = [];
        // this.world = {};
    }
}

export default Box2DPhysicsEngine;