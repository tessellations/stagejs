
class IPhysicsEngine
{
    /** Create the robot representation at the specified location
     *
     * @param x
     * @param y
     * @param theta
     * @param length
     * @param width
     */
    createRobot(x, y, theta, length, width) {}

    /** Create a wall segment at the specified location
     *
     * @param start_x
     * @param start_y
     * @param end_x
     * @param end_y
     * @param width
     */
    createWall(start_x, start_y, end_x, end_y, width) {}

    createGoal(x, y, length, width) {}

    leftSensorTriggered() { return false; }

    rightSensorTriggered() { return false; }

    atGoal() { return false; }

    setRobotVelocity(v, o) {}

    getPose() { return {x: 0, y: 0, theta: 0}}

    destroy() {}
}

export default IPhysicsEngine;