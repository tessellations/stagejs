import React from 'react';
import { Stack } from '@mui/material';

import MatterPhysicsEngine from './MatterPhysicsEngine';
import PlanckPhysicsEngine from "./PlanckPhysicsEngine";
import Box2DPhysicsEngine from "./Box2DPhysicsEngine";

import ReactiveController from './ReactiveController';

import './styles.css'

class PhysicsBenchmark extends React.Component
{
    constructor(props) {
        super(props);

        this.manual_drive = false;
        
        // Maze layout
        // Mazes are stored as a list of vectors with the start and end point x and y coordinates
        // stored as a set of multiples of the grid resolution, so that the maze can be adjusted to
        // accommodate different types and sizes of robots
        // Coordinates are stored a set x0 y0 x1 y1 for each segment
        this.wall_color = "rgb(50, 50, 150)";
        this.maze_resolution = 80;
        this.maze_cells_x = 10;
        this.maze_cells_y = 8;

        this.maze_goal_x = 5;
        this.maze_goal_y = 4;
        this.maze_goal_width = 1;
        this.maze_goal_height = 1;
        this.maze_goal_color = "rgb(50, 150, 50)";
        this.maze_wall_width = 2;

        this.maze_walls = [
            [0, 2, 2, 2],
            [1, 1, 3, 1],
            [3, 1, 3, 6],
            [0, 3, 1, 3],
            [2, 3, 3, 3],
            [2, 3, 2, 4],
            [1, 4, 2, 4],
            [1, 5, 3, 5],
            [1, 5, 1, 7],
            [3, 2, 5, 2],
            [5, 2, 5, 1],
            [4, 1, 4, 0],
            [6, 0, 6, 2],
            [6, 1, 8, 1],
            [9, 0, 9, 2],
            [9, 2, 8, 2],
            [4, 3, 4, 7],
            [4, 3, 8, 3],
            [7, 3, 7, 2],
            [8, 3, 8, 4],
            [8, 4, 10, 4],
            [9, 3, 10, 3],
            [3, 6, 2, 6],
            [2, 6, 2, 8],
            [3, 7, 7, 7],
            [7, 7, 7, 4],
            [7, 5, 9, 5],
            [8, 6, 10, 6],
            [9, 6, 9, 7],
            [8, 7, 8, 8],
            [6, 3, 6, 5],
            [5, 5, 5, 6],
            [5, 5, 6, 5],
            [5, 6, 6, 6],
            [0, 0, 10, 0],
            [10, 0, 10, 8],
            [10, 8, 0, 8],
            [0, 8, 0, 0]
        ];

        // Canvas properties
        this.canvas_width = this.maze_cells_x * this.maze_resolution;
        this.canvas_height = this.maze_cells_y * this.maze_resolution;

        // Robot properties
        this.robot_length = 40;
        this.robot_width = 30;
        this.robot_color = "rgb(150, 150, 200)";

        // Initial position is the center of the first cell
        this.robot_initial_x = this.maze_resolution / 2;
        this.robot_initial_y = this.maze_resolution / 2;
        this.robot_initial_theta = 0;

        // Sensor properties
        this.center_sensor_x = (this.robot_length / 2);
        this.center_sensor_y = 0;
        this.center_sensor_radius = 15;
        this.center_sensor_color = "rgb(50,50,50)";

        // Physics engines
        this.enginesIndex = 0;
        this.engineInitialized = false;
        this.engines = [
            {
                name: "MatterJS",
                engine: new MatterPhysicsEngine(this.canvas_width, this.canvas_height),
                drive_v: 0.75,
                drive_o: -0.02,
                total_frame_count: 0,
                overrun_frame_count: 0,
                max_frame_time: Number.MIN_VALUE,
                min_frame_time: Number.MAX_VALUE,
                average_frame_time: Number.MAX_VALUE,
                all_frame_times: []
            },
            {
                name: "Planck",
                engine: new PlanckPhysicsEngine(this.canvas_width, this.canvas_height),
                drive_v: 40,
                drive_o: -0.75,
                total_frame_count: 0,
                overrun_frame_count: 0,
                max_frame_time: Number.MIN_VALUE,
                min_frame_time: Number.MAX_VALUE,
                average_frame_time: Number.MAX_VALUE,
                all_frame_times: []
            },
            {
                name: "Box2D",
                engine: new Box2DPhysicsEngine(this.canvas_width, this.canvas_height),
                drive_v: 0.3,
                drive_o: -0.75,
                total_frame_count: 0,
                overrun_frame_count: 0,
                max_frame_time: Number.MIN_VALUE,
                min_frame_time: Number.MAX_VALUE,
                average_frame_time: Number.MAX_VALUE,
                all_frame_times: []
            }
        ];
        this.simulation_complete = false;

        // Controller
        this.controller = {};
        this.controller_start_time = new Date();

        // FPS and utilization counting
        this.frame_interval_count = 500;
        this.state = {
            max_frame_time: Number.MIN_VALUE,
            min_frame_time: Number.MAX_VALUE,
            total_frame_count: 0,
            total_frame_time: 0,
            all_frame_times: Array(this.frame_interval_count).fill(0),
            overrun_frame_count: 0,
            last_frame_time: new Date(),
            center_sensor_triggered: false,
            at_goal: false,
            fileDownloadUrl: ""
        };

        this.control = this.control.bind(this);
        this.initializeEngine = this.initializeEngine.bind(this);
        this.computeCurrentFramerate = this.computeCurrentFramerate.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.componentWillUnmount = this.componentWillUnmount.bind(this);
        this.handleKeypress = this.handleKeypress.bind(this);
        this.draw = this.draw.bind(this);
        this.centerSensorTriggered = this.centerSensorTriggered.bind(this);
        this.setVelocity = this.setVelocity.bind(this);
        this.createOutputFile = this.createOutputFile.bind(this);
        this.render = this.render.bind(this);
    }

    async control() {
        const timer = ms => new Promise(res => setTimeout(res, ms))

        for(let i = 0; i < this.engines.length; ) {
            const engine = this.engines[i].engine;
            this.enginesIndex = i;

            // Load pose
            const pose = engine.getPose();
            const x = pose.x;
            const y = pose.y;
            const theta = pose.theta;
            // console.log(`x${x} y${y} t${theta}`)

            // Exit condition
            if(this.atGoal(x, y, theta)) {
                // Save the time constants
                this.engines[i].max_frame_time = this.state.max_frame_time;
                this.engines[i].min_frame_time = this.state.min_frame_time;
                this.engines[i].average_frame_time = this.state.total_frame_time / this.state.total_frame_count;
                this.engines[i].all_frame_times = this.state.all_frame_times;
                this.engines[i].total_frame_count = this.state.total_frame_count;
                this.engines[i].overrun_frame_count = this.state.overrun_frame_count;

                // Reset time constraints
                this.setState({
                    at_goal: true,
                    max_frame_time: Number.MIN_VALUE,
                    min_frame_time: Number.MAX_VALUE,
                    total_frame_count: 0,
                    total_frame_time: 0,
                    all_frame_times: Array(this.frame_interval_count).fill(0),
                    last_frame_time: new Date()
                });

                // Kill the current engine
                engine.destroy();

                // Remove all traces that the current engine ever existed
                this.engines[i].engine = {};

                // Kill the controller too
                this.controller = {};

                // Update the engine
                i++;
                this.engineInitialized = false;
                continue;
            }

            // Create the objects in the engine if they haven't been created yet
            if(!this.engineInitialized)
                this.initializeEngine();

            // Run the controller
            if(!this.manual_drive)
                this.controller.loop();

            // Update the canvas
            this.draw(x, y, theta);

            // Update the sensor state
            this.setState({
                center_sensor_triggered: this.centerSensorTriggered(x, y, theta)
            });

            // Update frame count
            this.computeCurrentFramerate();

            // Delay
            await timer(1);
        }

        this.simulation_complete = true;
        console.log(`Simulation complete, control() loop terminating`);

        this.createOutputFile();
    }

    initializeEngine() {
        // Load the current engine
        const engine = this.engines[this.enginesIndex].engine;

        // Create the controller
        this.controller = new ReactiveController(this.maze_cells_x,
                                                 this.maze_cells_y,
                                                 this.maze_resolution,
                                                 this.engines[this.enginesIndex].drive_v,
                                                 this.engines[this.enginesIndex].drive_o,
                                                 this.setVelocity.bind(this),
                                                 () => { return this.state.center_sensor_triggered },
                                                 () => {
                                                     const engine = this.engines[this.enginesIndex].engine;
                                                     return engine.getPose();
                                                 });

        this.controller_start_time = new Date();
        this.controller.setup();

        engine.createRobot(this.robot_initial_x,
                        this.canvas_height - this.robot_initial_y,
                           this.robot_initial_theta,
                           this.robot_length,
                           this.robot_width);

        for(let i = 0; i < this.maze_walls.length; i++)
        {
            const wall = this.maze_walls[i];

            engine.createWall(this.maze_resolution * wall[0],
                              this.canvas_height - this.maze_resolution * wall[1],
                              this.maze_resolution * wall[2],
                              this.canvas_height - this.maze_resolution * wall[3],
                              this.maze_wall_width);
        }

        engine.start();

        this.engineInitialized = true;

        // Reset completion indicator
        this.setState({at_goal: false});
    }

    computeCurrentFramerate() {
        // Get time delta since last frame
        const delta_ms = ((new Date().getTime()) - this.state.last_frame_time.getTime());

        const floored_delta = Math.floor(delta_ms);

        if(floored_delta > -1 && floored_delta < this.frame_interval_count) {
            const new_frame_times = this.state.all_frame_times.map((count, index) => {
                if(index === floored_delta)
                    return count + 1;
                else
                    return count;
            });
            this.setState({all_frame_times: new_frame_times});
        }
        else {
            this.setState({overrun_frame_count: this.state.overrun_frame_count + 1});
        }

        this.setState({
            max_frame_time: Math.max(this.state.max_frame_time, delta_ms),
            min_frame_time: Math.min(this.state.min_frame_time, delta_ms),
            total_frame_time: this.state.total_frame_time + delta_ms,
            total_frame_count: this.state.total_frame_count + 1,
            last_frame_time: new Date()
        });
    }

    draw(x, y, theta) {
        const canvas = document.getElementById("canvas");
        const context = canvas ? canvas.getContext("2d") : null;

        if(context)
        {
            // Clear the canvas
            context.clearRect(0, 0, this.canvas_width, this.canvas_height);

            // Render nothing if complete
            if(this.simulation_complete) {
                console.log(`Simulation complete, skipping draw()`);
                return;
            }

            // Draw goal
            this.drawRectangle(context,
                               this.maze_goal_color,
                               this.maze_resolution * this.maze_goal_x,
                               this.maze_resolution * this.maze_goal_y,
                               0,
                               this.maze_resolution * this.maze_goal_height,
                               this.maze_resolution * this.maze_goal_width);

            // Draw walls
            for(let i = 0; i < this.maze_walls.length; i++)
            {
                const segment = this.maze_walls[i];
                this.drawLine(context,
                              this.wall_color,
                              this.maze_resolution * segment[0],
                              this.canvas_height - (this.maze_resolution * segment[1]),
                              this.maze_resolution * segment[2],
                              this.canvas_height - (this.maze_resolution * segment[3]));
            }

            // Draw sensors
            this.drawSensor(context, this.center_sensor_color, x, y, theta, this.center_sensor_x, this.center_sensor_y, this.center_sensor_radius);

            // Draw robot
            this.drawRectangle(context, this.robot_color, x, y, theta, this.robot_length, this.robot_width);
        }
    }

    drawRectangle(context, color, x, y, theta, length_x, width_y) {
        const radius = Math.sqrt((length_x / 2) * (length_x / 2) + (width_y / 2) * (width_y / 2));
        const angle = Math.atan(width_y / length_x);

        const p0 = {
            x: x + radius * Math.cos(angle + theta),
            y: y + radius * Math.sin(angle + theta)
        };

        const p1 = {
            x: x - radius * Math.cos(angle - theta),
            y: y + radius * Math.sin(angle - theta)
        };

        const p2 = {
            x: x - radius * Math.cos(angle + theta),
            y: y - radius * Math.sin(angle + theta)
        };

        const p3 = {
            x: x + radius * Math.cos(angle - theta),
            y: y - radius * Math.sin(angle - theta)
        };

        const points = [p0, p1, p2, p3];
        context.fillStyle = color;

        for(let i = 0; i < points.length; i++)
        {
            for(let j = 0; j < points.length; j++)
            {
                if(i === j)
                    continue;
                else
                {
                    context.beginPath();
                    context.moveTo(x, y);
                    context.lineTo(points[i].x, points[i].y);
                    context.lineTo(points[j].x, points[j].y);
                    context.closePath();
                    context.fill();
                }
            }
        }
    }

    drawLine(context, color, x0, y0, x1, y1) {
        context.fillStyle = color;
        context.beginPath();
        context.moveTo(x0, y0);
        context.lineTo(x1, y1);
        context.closePath();
        context.stroke();
    }

    drawSensor(context, color, x, y, theta, x_offset, y_offset, radius) {
        const angle = Math.atan2(y_offset, x_offset);
        const distance = Math.sqrt(x_offset * x_offset + y_offset * y_offset);
        const adjustedX = x + distance * Math.cos(angle + theta);
        const adjustedY = y + distance * Math.sin(angle + theta);

        context.fillStyle = color;
        context.moveTo(adjustedX, adjustedY);
        context.arc(adjustedX, adjustedY, radius, 0, 2 * Math.PI);
        context.fill();
    }

    centerSensorTriggered(x, y, theta) {
        // Sensor seems to glitch and return true on startup sometimes. This prevents a collision being triggered for a short time after starting the engine
        if(new Date().getTime() - this.controller_start_time.getTime() < 500)
            return false;

        for(let i = 0; i < this.maze_walls.length; i++) {
            const wall = this.maze_walls[i];

            if(this.doesSensorIntersectLine(x, y, theta, this.center_sensor_x, this.center_sensor_y, this.center_sensor_radius, this.maze_resolution * wall[0], this.canvas_height - (this.maze_resolution * wall[1]), this.maze_resolution * wall[2], this.canvas_height - (this.maze_resolution * wall[3])))
                return true;
        }

        return false;
    }

    /** A workaround for the limits of Matter.JS at this point in time. Collision detection using matter isn't working at this point in time
     * because in the current version a) circular and polygon shapes cause errors, and b) creating composite bodies with constraints also causes
     * errors.
     */
    doesSensorIntersectLine(x, y, theta, x_offset, y_offset, sensor_radius, x0, y0, x1, y1) {
        const angle = Math.atan2(y_offset, x_offset);
        const distance = Math.sqrt(x_offset * x_offset + y_offset * y_offset);
        const sensorCenterX = x + distance * Math.cos(angle + theta);
        const sensorCenterY = y + distance * Math.sin(angle + theta);

        // Define some common functions for working with vectors
        const add = (a, b) => ({x: a.x + b.x, y: a.y + b.y});
        const sub = (a, b) => ({x: a.x - b.x, y: a.y - b.y});
        const dot = (a, b) => a.x * b.x + a.y * b.y;
        const hypot2 = (a, b) => dot(sub(a, b), sub(a, b));

        // Function for projecting some vector a onto b
        function proj(a, b) {
            const k = dot(a, b) / dot(b, b);
            return {x: k * b.x, y: k * b.y};
        }

        function distanceSegmentToPoint(A, B, C) {
            // Compute vectors AC and AB
            const AC = sub(C, A);
            const AB = sub(B, A);

            // Get point D by taking the projection of AC onto AB then adding the offset of A
            const D = add(proj(AC, AB), A);

            const AD = sub(D, A);
            // D might not be on AB so calculate k of D down AB (aka solve AD = k * AB)
            // We can use either component, but choose larger value to reduce the chance of dividing by zero
            const k = Math.abs(AB.x) > Math.abs(AB.y) ? AD.x / AB.x : AD.y / AB.y;

            // Check if D is off either end of the line segment
            if (k <= 0.0) {
                return Math.sqrt(hypot2(C, A));
            } else if (k >= 1.0) {
                return Math.sqrt(hypot2(C, B));
            }

            return Math.sqrt(hypot2(C, D));
        }

        return Math.abs(distanceSegmentToPoint({x: x0, y: y0}, {x: x1, y: y1}, {x: sensorCenterX, y: sensorCenterY})) < sensor_radius;
    }

    atGoal(x, y, theta) {
        const goal_x = this.maze_resolution * this.maze_goal_x;
        const goal_y = this.canvas_height - (this.maze_resolution * this.maze_goal_y);

        const goal_upper = {
            x: goal_x + (this.maze_resolution * this.maze_goal_width / 2),
            y: goal_y + (this.maze_resolution * this.maze_goal_height / 2),
        };
        const goal_lower = {
            x: goal_x - (this.maze_resolution * this.maze_goal_width / 2),
            y: goal_y - (this.maze_resolution * this.maze_goal_height / 2),
        };

        const radius = Math.sqrt((this.robot_length / 2) * (this.robot_length / 2) + (this.robot_width / 2) * (this.robot_width / 2));
        const angle = Math.atan(this.robot_width / this.robot_length);

        const robot0 = {
            x: x + radius * Math.cos(angle + theta),
            y: y + radius * Math.sin(angle + theta)
        };

        const robot1 = {
            x: x - radius * Math.cos(angle - theta),
            y: y + radius * Math.sin(angle - theta)
        };

        const robot2 = {
            x: x - radius * Math.cos(angle + theta),
            y: y - radius * Math.sin(angle + theta)
        };

        const robot3 = {
            x: x + radius * Math.cos(angle - theta),
            y: y - radius * Math.sin(angle - theta)
        };

        const robot_points = [robot0, robot1, robot2, robot3];

        for(let i = 0; i < robot_points.length; i++) {
            const point = robot_points[i];

            if(point.x < goal_upper.x && point.x > goal_lower.x && point.y < goal_upper.y && point.y > goal_lower.y)
                return true;
        }

        return false;
    }

    componentDidMount() {
        window.addEventListener("keypress", this.handleKeypress);

        setTimeout(this.control, 1);
    }

    componentWillUnmount() {
        window.removeEventListener("keypress", this.handleKeypress);
    }

    // Binding for the controller to make use of
    setVelocity(v, o) {
        const engine = this.engines[this.enginesIndex].engine;
        engine.setRobotVelocity(v, o);
    }

    // Creates the output file for download
    createOutputFile() {
        let data = [];

        // Create header / summary data
        let first = 'Engine';
        let second = 'Max (ms)';
        let third = 'Min (ms)';
        let fourth = `Count above ${this.frame_interval_count}ms`;

        for(let j = 0; j < this.engines.length; j++) {
            first += `,${this.engines[j].name}`;
            second += `,${this.engines[j].max_frame_time.toString()}`
            third += `,${this.engines[j].min_frame_time.toString()}`
            fourth += `,${this.engines[j].overrun_frame_count.toString()}`
        }

        first += '\r\n';
        second += '\r\n';
        third += '\r\n';
        fourth += '\r\n';

        data.push(first);
        data.push(second);
        data.push(third);
        data.push(fourth);
        data.push(',\r\n');

        for(let i = 0; i < this.frame_interval_count; i++) {
            let row = `${i}`;

            for(let j = 0; j < this.engines.length; j++) {
                row += `,${this.engines[j].all_frame_times[i] / this.engines[j].total_frame_count}`;
            }

            row += '\r\n';
            data.push(row);
        }

        // Create blob
        const blob = new Blob(data, {type:'text/plain'});
        this.setState({fileDownloadUrl: URL.createObjectURL(blob)});
    }

    handleKeypress(event) {
        const key = event.code.toString().replace('Key', '').toLowerCase();
        const engine = this.engines[this.enginesIndex].engine;
        const v = this.engines[this.enginesIndex].drive_v;
        const o = this.engines[this.enginesIndex].drive_o;

        switch(key)
        {
            case 'q':
                this.manual_drive = !this.manual_drive;
                break;
            case 'w':
                if(this.manual_drive)
                    engine.setRobotVelocity(v, 0);
                break;
            case 's':
                if(this.manual_drive)
                    engine.setRobotVelocity(-v, 0);
                break;
            case 'a':
                if(this.manual_drive)
                    engine.setRobotVelocity(0, o);
                break;
            case 'd':
                if(this.manual_drive)
                    engine.setRobotVelocity(0, -o);
                break;
            case 'space':
                engine.setRobotVelocity(0, 0);
                break;
            default:
                console.log(`Unhandled key press for key ${key}`);
                break;
        }
    }

    render() {
        const downloadButton = this.state.fileDownloadUrl.length === 0 ? <p /> : <button className={"download-button"} onClick={() => {
            const link = document.createElement('a');
            link.download = `physics_simulation_results_${new Date().toISOString().replace(':', '.')}.csv`;
            link.href = this.state.fileDownloadUrl;
            link.addEventListener('click', (e) => {
                setTimeout(() => URL.revokeObjectURL(link.href), 30 * 1000);
            });
            link.click();
            this.setState({fileDownloadUrl: ""});
        }}>
            Download Performance Data
        </button>;

        const infoComponents = this.simulation_complete ? [] : [
            <p key={"1"}>Currently evaluating: {this.engines[this.enginesIndex].name}</p>,
            <br key={"2"}/>,
            <p key={"3"}>Average frame time: {this.state.total_frame_time / this.state.total_frame_count}</p>,
            <p key={"4"}>Maximum frame time: {this.state.max_frame_time}</p>,
            <p key={"5"}>Minimum frame time: {this.state.min_frame_time}</p>,
            <br key={"6"} />,
            <Stack key={"7"} direction={"row"} alignItems={"flex-start"} justifyContent={"flex-start"} spacing={2}>
                <div className={this.state.center_sensor_triggered ? "green-indicator" : "gray-indicator"} />
            </Stack>,
            <br key={"8"}/>,
            <p key={"9"}>{this.state.at_goal ? "Done! Cleaning up..." : ""}</p>,
            <p key={"10"}>{this.manual_drive ? "Manual drive enabled" : ""}</p>
        ];

        return (
        <div className={"container"}>
            <Stack direction={"row"} alignItems={"flex-start"} justifyContent={"flex-start"} spacing={2}>
                <canvas id={"canvas"} width={this.canvas_width} height={this.canvas_height} />
                <Stack direction={"column"} alignItems={"flex-start"} justifyContent={"flex-start"} spacing={2}>
                    {infoComponents}
                    <br />
                    {downloadButton}
                </Stack>
            </Stack>
        </div>)
    }
}

export default PhysicsBenchmark;