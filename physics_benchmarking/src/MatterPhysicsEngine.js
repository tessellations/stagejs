import Matter from "matter-js";

class MatterPhysicsEngine
{
    constructor(screen_width, screen_height) {
        // Create engine
        this.engine = Matter.Engine.create({});
        this.engine.world.gravity.y = 0;

        // References
        this.wall_refs = [];
        this.robot_ref = {};

        // Bindings
        this.createRobot = this.createRobot.bind(this);
        this.createWall = this.createWall.bind(this);
        this.start = this.start.bind(this);
        this.setRobotVelocity = this.setRobotVelocity.bind(this);
        this.getPose = this.getPose.bind(this);
        this.destroy = this.destroy.bind(this);
    }

    createRobot(x,
                y,
                theta,
                length,
                width)
    {
        // Create the robot 'frame'
        this.robot_ref = Matter.Bodies.rectangle(x, y, length, width);
        Matter.Body.setAngle(this.robot_ref, theta);
    }

    createWall(start_x, start_y, end_x, end_y, width) {
        if(start_x === end_x) {
            const newWall = Matter.Bodies.rectangle(
                (start_x + end_x) / 2,
                (start_y + end_y) / 2,
                width,
                Math.abs(start_y - end_y),
                {
                    isStatic: true,
                });

            this.wall_refs.push(newWall);
        } else if(start_y === end_y) {
            const newWall = Matter.Bodies.rectangle(
                (start_x + end_x) / 2,
                (start_y + end_y) / 2,
                Math.abs(start_x - end_x),
                width,
                {
                    isStatic: true,
                });

            this.wall_refs.push(newWall);
        } else {
            // console.log(`Invalid wall placement: ${start_x} ${start_y} ${end_x} ${end_y}`)
        }
    }

    start() {
        Matter.World.add(this.engine.world, this.robot_ref);
        Matter.World.add(this.engine.world, this.wall_refs);

        Matter.Runner.run(this.engine);
    }

    setRobotVelocity(v, o) {
        if(this.robot_ref && this.robot_ref.position)
        {
            const theta = this.robot_ref.angle + (2.0 * Math.PI);
            Matter.Body.setVelocity(this.robot_ref, {x: Math.cos(theta) * v, y: Math.sin(theta) * v});
            Matter.Body.setAngularVelocity(this.robot_ref, o);
        } else {
          console.warn(`Failed to set robot velocity`);
        }
    }

    getPose() {
        if(this.robot_ref && this.robot_ref.position) {
            return {
                x: this.robot_ref.position.x,
                y: this.robot_ref.position.y,
                theta: this.robot_ref.angle
            }
        }
        else
            return {
                x: 0,
                y: 0,
                theta: 0
            }
    }

    destroy() {
        this.wall_refs = [];
        this.robot_ref = {};
        this.engine = {};
    }
}

export default MatterPhysicsEngine;