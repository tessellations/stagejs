/** A basic reactive controller implementation. In the future, this hardcoded solution can
 *  be replaced with js-interpreter and native functions executed by the controller. For now,
 *  this process will be emulated using a set of callbacks that the caller can use to make
 *  decisions about navigation.
 */
class ReactiveController
{
    constructor(maze_cells_x,
                maze_cells_y,
                maze_resolution,
                v,
                o,
                setVelocity,
                centerSensorPressed,
                getPose) {
        this.setVelocity = setVelocity;
        this.centerSensorPressed = centerSensorPressed;
        this.getPose = getPose;

        // State control
        this.states = {
            CONTROL_YAW: 1,
            CONTROL_POSITION: 2,
            TRY_NORTH: 3,
            TRY_EAST: 4,
            TRY_SOUTH: 5,
            TRY_WEST: 6
        };
        this.state = this.states.CONTROL_POSITION;

        // Occupancy grid
        this.maze_cells_x = maze_cells_x;
        this.maze_cells_y = maze_cells_y;
        this.maze_resolution = maze_resolution;
        this.grid = new Array(maze_cells_x * maze_cells_y);
        for(let i = 0; i < maze_cells_x; i++) {
            for(let j = 0; j < maze_cells_y; j++) {
                this.grid[i * maze_cells_x + j] = {
                    n: true,
                    e: true,
                    s: true,
                    w: true,
                    visited: false
                }
            }
        }

        // Driving speed constants
        this.v = v;
        this.o = o;

        // Other constants
        this.target = {
            x: 0,
            y: 0,
            theta: 0
        };

        this.tolerance = {
            x: 5,
            y: 5,
            theta: 0.02 // 1 degree
        }

        // Bindings
        this.setup = this.setup.bind(this);
        this.loop = this.loop.bind(this);
        this.getGridCell = this.getGridCell.bind(this);
        this.getSurroundingCells = this.getSurroundingCells.bind(this);
        this.getNewState = this.getNewState.bind(this);
    }

    // Setup method will be called once by the simulator at creation
    setup() {
        this.setVelocity(0, 0);
    }

    // Helper to bound theta angle from 0 to 2PI
    boundTheta(inputTheta) {
        if(inputTheta > (2 * Math.PI))
            return this.boundTheta(inputTheta - (2 * Math.PI));
        else if(inputTheta < 0)
            return this.boundTheta(inputTheta + (2 * Math.PI));
        else
            return inputTheta;
    }

    // Determines the cell in the grid that the robot is in
    getMazeCellIndex(pose) {
        return {
            x: Math.floor(pose.x / this.maze_resolution),
            y: Math.floor(pose.y / this.maze_resolution),
        }
    }

    // Helper to get a grid cell at an index. Returns null if the cell does not exist
    getGridCell(x_index, y_index) {
        return (x_index * this.maze_cells_x) + y_index < this.grid.length ? this.grid[(x_index * this.maze_cells_x) + y_index] : undefined;
    }

    // Helper to get the surrounding cells at cardinal directions
    getSurroundingCells(x_index, y_index) {
        return {
            north: this.getGridCell(x_index, y_index - 1),
            east: this.getGridCell(x_index + 1, y_index),
            south: this.getGridCell(x_index, y_index + 1),
            west: this.getGridCell(x_index - 1, y_index)
        };
    }

    // Function to determine the next state. This simplifies the state determination in the control loop
    // Returns undefined if there is an error
    getNewState(surroundingCells, collisionDirection) {
        const directions = ['n', 'e', 's', 'w'];

        // Assert validity
        let newState = undefined;
        for(let i = 0; i < directions.length; i++) {
            const direction = directions[i];

            if(direction === collisionDirection) {
                // Status of cells to help clarify if statements
                const northExists = surroundingCells.hasOwnProperty('north') && surroundingCells.north !== undefined;
                const eastExists = surroundingCells.hasOwnProperty('east') && surroundingCells.east !== undefined;
                const southExists = surroundingCells.hasOwnProperty('south') && surroundingCells.south !== undefined;
                const westExists = surroundingCells.hasOwnProperty('west') && surroundingCells.west !== undefined;

                const northUnvisited = northExists ? surroundingCells.north.visited === false : false;
                const eastUnvisited = eastExists ? surroundingCells.east.visited === false : false;
                const southUnvisited = southExists ? surroundingCells.south.visited === false : false;
                const westUnvisited = westExists ? surroundingCells.west.visited === false : false;

                const northTraversable = northExists ? surroundingCells.north.n === true : false;
                const eastTraversable = eastExists ? surroundingCells.east.e === true : false;
                const southTraversable = southExists ? surroundingCells.south.s === true : false;
                const westTraversable = westExists ? surroundingCells.west.w === true : false;


                // First, prioritize directions that haven't been explored
                for(let j = 1; j < directions.length; j++) {
                    const nextDirection = directions[(i + j) % directions.length];

                    if(nextDirection === 'n' && northExists && northUnvisited && northTraversable) newState = this.states.TRY_NORTH;
                    else if(nextDirection === 'e' && eastExists && eastUnvisited && eastTraversable) newState = this.states.TRY_EAST;
                    else if(nextDirection === 's' && southExists && southUnvisited && southTraversable) newState = this.states.TRY_SOUTH;
                    else if(nextDirection === 'w' && westExists && westUnvisited && westTraversable) newState = this.states.TRY_WEST;
                }

                // Exit if we are done
                if(newState !== undefined) {
                    return newState;
                }
                // Pick the next open direction
                else {
                    for(let j = 1; j < directions.length; j++) {
                        const nextDirection = directions[(i + j) % directions.length];

                        if(nextDirection === 'n' && northExists && northTraversable) newState = this.states.TRY_NORTH;
                        else if(nextDirection === 'e' && eastExists && eastTraversable) newState = this.states.TRY_EAST;
                        else if(nextDirection === 's' && southExists && southTraversable) newState = this.states.TRY_SOUTH;
                        else if(nextDirection === 'w' && westExists && westTraversable) newState = this.states.TRY_WEST;
                    }

                    return newState;
                }
            }
        }

        // Default case - no match for direction
        console.log(`Invalid direction provided to getNewState: ${collisionDirection}`);
        return newState;
    }

    // This function will be called repeatedly during execution. Do not loop / block in this function
    loop() {
        // State determination
        const center = this.centerSensorPressed();
        const pose = this.getPose();
        const theta = this.boundTheta(pose.theta);

        // Update the state of the current cell to be explored
        const currentCell = this.getMazeCellIndex(pose);
        this.grid[currentCell.x * this.maze_cells_x + currentCell.y].visited = true;

        // State control
        if(this.state === this.states.CONTROL_YAW) {
            // Exit case
            const atTarget = (Math.abs(this.boundTheta(this.target.theta - theta)) < this.tolerance.theta);

            if(atTarget) {
                console.log(`Yaw control complete, starting drive! Theta: ${theta}`);
                this.state = this.states.CONTROL_POSITION;
            } else {
                const error = this.boundTheta(this.target.theta - theta);

                if(error < 0) {
                    this.setVelocity(0, -this.o);
                } else {
                    this.setVelocity(0, this.o);
                }
            }
        } else if(this.state === this.states.CONTROL_POSITION) {
            // Exit case
            const yawNeedsCorrection = (Math.abs(this.boundTheta(this.target.theta - theta)) > this.tolerance.theta);

            if(center) {
                if(theta > (Math.PI / 4) && theta < (3 * Math.PI / 4)) {
                    console.log(`South-facing collision detected`);
                    this.grid[currentCell.x * this.maze_cells_x + currentCell.y].s = false;

                    // Get the surrounding cells
                    const cells = this.getSurroundingCells(currentCell.x, currentCell.y);
                    const newState = this.getNewState(cells, 's');

                    if(newState !== undefined)
                        this.state = newState;
                    else
                        console.log(`Error: invalid next state returned`);

                } else if(theta > (3 * Math.PI / 4) && theta < (5 * Math.PI / 4)) {
                    console.log(`West-facing collision detected`);
                    this.grid[currentCell.x * this.maze_cells_x + currentCell.y].w = false;

                    // Get the surrounding cells
                    const cells = this.getSurroundingCells(currentCell.x, currentCell.y);
                    const newState = this.getNewState(cells, 'w');

                    if(newState !== undefined)
                        this.state = newState;
                    else
                        console.log(`Error: invalid next state returned`);
                } else if(theta > (5 * Math.PI / 4) && theta < (7 * Math.PI / 4)) {
                    console.log(`North-facing collision detected`);
                    this.grid[currentCell.x * this.maze_cells_x + currentCell.y].n = false;

                    // Get the surrounding cells
                    const cells = this.getSurroundingCells(currentCell.x, currentCell.y);
                    const newState = this.getNewState(cells, 'n');

                    if(newState !== undefined)
                        this.state = newState;
                    else
                        console.log(`Error: invalid next state returned`);
                } else {
                    console.log(`East-facing collision detected`);
                    this.grid[currentCell.x * this.maze_cells_x + currentCell.y].e = false;

                    // Get the surrounding cells
                    const cells = this.getSurroundingCells(currentCell.x, currentCell.y);
                    const newState = this.getNewState(cells, 'e');

                    if(newState !== undefined)
                        this.state = newState;
                    else
                        console.log(`Error: invalid next state returned`);
                }
            } else if(yawNeedsCorrection) {
                console.log(`Yaw error, stopping to correct`);
                this.state = this.states.CONTROL_YAW;
            } else {
                // Handle case
                this.setVelocity(this.v, 0);
            }
        } else if(this.state === this.states.TRY_NORTH) {
            console.log(`Trying cell to the north`);

            const currentCell = this.getMazeCellIndex(pose);
            this.target = {
                x: (currentCell.x) * this.maze_resolution,
                y: (currentCell.y - 1) * this.maze_resolution,
                theta: -Math.PI / 2
            };

            this.state = this.states.CONTROL_YAW;
        } else if(this.state === this.states.TRY_EAST) {
            console.log(`Trying cell to the east`);

            const currentCell = this.getMazeCellIndex(pose);
            this.target = {
                x: (currentCell.x + 1) * this.maze_resolution,
                y: (currentCell.y) * this.maze_resolution,
                theta: 0
            };

            this.state = this.states.CONTROL_YAW;
        } else if(this.state === this.states.TRY_SOUTH) {
            console.log(`Trying cell to the south`);

            const currentCell = this.getMazeCellIndex(pose);
            this.target = {
                x: (currentCell.x) * this.maze_resolution,
                y: (currentCell.y + 1) * this.maze_resolution,
                theta: Math.PI / 2
            };

            this.state = this.states.CONTROL_YAW;
        } else if(this.state === this.states.TRY_WEST) {
            console.log(`Trying cell to the west`);

            const currentCell = this.getMazeCellIndex(pose);
            this.target = {
                x: (currentCell.x - 1) * this.maze_resolution,
                y: (currentCell.y) * this.maze_resolution,
                theta: Math.PI
            };

            this.state = this.states.CONTROL_YAW;
        } else {
            console.log(`Error: unhandled state in controller: ${this.state}`);
        }
    }
}

export default ReactiveController;