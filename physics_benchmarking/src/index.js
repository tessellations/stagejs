import React from 'react';
import ReactDOM from 'react-dom/client';
import PhysicsBenchmark from './PhysicsBenchmark';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <PhysicsBenchmark />
  </React.StrictMode>
);
