import { World, Vec2, Box, Edge } from "planck";

class PlanckPhysicsEngine
{
    constructor(screen_width, screen_height) {
        // Create world
        this.world = new World({
            gravity: Vec2(0, 0),
            allowSleep: true
        });

        this.interval = {};
        this.timeStep = 0.02;
        this.velocityIterations = 6;
        this.positionIterations = 2;

        // Bindings
        this.createRobot = this.createRobot.bind(this);
        this.createWall = this.createWall.bind(this);
        this.start = this.start.bind(this);
        this.setRobotVelocity = this.setRobotVelocity.bind(this);
        this.getPose = this.getPose.bind(this);
        this.destroy = this.destroy.bind(this);
    }

    createRobot(x,
                y,
                theta,
                length,
                width)
    {
        let robot = this.world.createBody();
        robot.setDynamic();
        robot.setPosition(Vec2(x, y));
        robot.setAngle(theta);
        robot.setMassData({
            mass: 1,
            center: Vec2(),
            I: 1
        });
        robot.createFixture(Box(length / 2, width / 2));

        this.robot_ref = robot;
    }

    createWall(start_x, start_y, end_x, end_y, width) {
        if(!this.wall_refs)
            this.wall_refs = [];

        const new_wall = this.world.createBody({ type: "static" }).createFixture(Edge(Vec2(start_x, start_y), Vec2(end_x,  end_y)));
        this.wall_refs.push(new_wall);
    }

    start() {
        // Run
        this.interval = setInterval(() => {
            // Because there doesn't seem to be an option for air friction, the robot will never slow down.
            // This is an attempt to replicate that by gradually reducing the velocity of the robot at each step
            const factor = 0.98;
            const linear_velocity = this.robot_ref.getLinearVelocity();
            const angular_velocity = this.robot_ref.getAngularVelocity();

            this.robot_ref.setLinearVelocity(linear_velocity.mul(factor));
            this.robot_ref.setAngularVelocity(angular_velocity * factor);

            this.world.step(this.timeStep, this.velocityIterations, this.positionIterations);
        }, this.timeStep * 1000);
    }

    setRobotVelocity(v, o) {
        if(this.robot_ref) {
            const theta = this.robot_ref.getAngle() + (2.0 * Math.PI);
            this.robot_ref.setLinearVelocity(Vec2(Math.cos(theta) * v, Math.sin(theta) * v));
            this.robot_ref.setAngularVelocity(o);
        } else {
            console.warn(`Failed to set robot velocity`);
        }
    }

    getPose() {
        if(this.robot_ref) {
            const poseXY = this.robot_ref.getPosition();
            const theta = this.robot_ref.getAngle();
            return {
                x: poseXY.x,
                y: poseXY.y,
                theta: theta
            }
        } else {
            return {
                x: 0,
                y: 0,
                theta: 0
            }
        }
    }

    destroy() {
        this.robot_ref = {};
        this.wall_refs = [];
        this.world = {};
        this.runner = {};
        clearInterval(this.interval);
    }
}

export default PlanckPhysicsEngine;